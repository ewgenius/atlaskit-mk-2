import { UploadParams } from './config';

export const defaultUploadParams: UploadParams = {
  collection: '',
  fetchMetadata: true,
  autoFinalize: true,
};
