export const isImage = function(type: any) {
  return ['image/jpeg', 'image/png'].indexOf(type) > -1;
};
