// @flow

import AnalyticsEvent from './AnalyticsEvent';
import UIAnalyticsEvent from './UIAnalyticsEvent';

// Utils
export type ObjectType = { [string]: any };

// Basic events
export type AnalyticsEventPayload = {
  action: string,
  [string]: any,
};

export type AnalyticsEventUpdater =
  | ObjectType
  | ((payload: AnalyticsEventPayload) => AnalyticsEventPayload);

export type AnalyticsEventProps = {
  payload: AnalyticsEventPayload,
};

export interface AnalyticsEventInterface {
  payload: AnalyticsEventPayload;

  clone: () => AnalyticsEvent;
  update(updater: AnalyticsEventUpdater): AnalyticsEvent;
}

// UI events
type ChannelIdentifier = string;

export type UIAnalyticsEventHandlerSignature = (
  event: UIAnalyticsEvent,
  channel?: ChannelIdentifier,
) => void;

export type UIAnalyticsEventProps = AnalyticsEventProps & {
  context?: Array<ObjectType>,
  handlers?: Array<UIAnalyticsEventHandlerSignature>,
};

export interface UIAnalyticsEventInterface {
  context: Array<ObjectType>;
  handlers?: Array<UIAnalyticsEventHandlerSignature>;
  hasFired: boolean;
  payload: AnalyticsEventPayload;

  clone(): UIAnalyticsEvent | null;
  fire(channel?: ChannelIdentifier): void;
  update(updater: AnalyticsEventUpdater): AnalyticsEvent;
}
