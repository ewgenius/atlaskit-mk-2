// @flow

import Async from 'react-select/lib/Async';
import createSelect from './createSelect';

export default createSelect(Async);
