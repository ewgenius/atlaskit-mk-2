# Select
React component which allows selection of an item or items from a dropdown list. Substitute for the native select element

## Try it out

Detailed docs and example usage can be found [here](https://atlaskit.atlassian.com).
