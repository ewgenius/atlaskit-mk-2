# Button

Buttons are used as triggers for actions. They are used in forms, toolbars,
dialog footers and as stand-alone action triggers.

## Installation

```sh
npm install @atlaskit/button
```

## Using the component

### HTML

This package exports the `@NAME@` React component.

Import the component in your React app as follows:

```javascript
import Button from '@atlaskit/button';
ReactDOM.render(<Button />, container);
```
