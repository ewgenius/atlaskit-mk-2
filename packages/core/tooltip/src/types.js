// @flow

export type PositionType = 'bottom' | 'left' | 'right' | 'top';
export type CoordinatesType = { left?: number, top?: number };
