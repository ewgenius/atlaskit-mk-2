# Layer Manager

The layer manager is used to render React DOM into a new context (aka "Portal"). This can be used to implement various UI components such as modals.
