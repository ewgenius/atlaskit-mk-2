# Tag

This component is displayed as an accessible tag with an optional link and/or
button to remove it.

![Example tags](https://i.imgur.com/UPtxaIM.png)

Although the `tag` component can be used by itself, it works best in
conjunction with the [`@atlaskit/tag-group`](https://www.npmjs.com/package/@atlaskit/tag-group)
component.
