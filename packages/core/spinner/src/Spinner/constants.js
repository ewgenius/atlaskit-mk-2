// @flow
export const SIZES_MAP = {
  small: 20,
  medium: 30,
  large: 50,
  xlarge: 100,
};
export const DEFAULT_SIZE = SIZES_MAP.small;
