# @atlaskit/size-detector

## 2.2.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 2.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 2.1.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.1.2
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 2.1.1

- [patch] Migrate Navigation from Ak repo to ak mk 2 repo, Fixed flow typing inconsistencies in ak mk 2 [bdeef5b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bdeef5b)

## 2.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.0.4

## 2.0.3
- [patch] Minor adjustment to size-detector to satisfy migration requirements [a5fb97d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a5fb97d)
- [patch] migrated to atlaskit-mk-2 [c606eb2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c606eb2)

## 2.0.2 (2017-10-22)

* bug fix; update styled-components dep and react peerDep ([6a67bf8](https://bitbucket.org/atlassian/atlaskit/commits/6a67bf8))
## 2.0.1 (2017-10-03)

* bug fix; Fix incorrect propType declaration ([46d1f50](https://bitbucket.org/atlassian/atlaskit/commits/46d1f50))
