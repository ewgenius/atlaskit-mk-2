# Size detector

This is a utility component that informs the child function of the available width and height.

## Try it out

Interact with a [live demo of the @atlaskit/size-detector component](https://atlaskit.atlassian.com).

## Installation

```sh
yarn add @atlaskit/size-detector
```

## Acknowledgements

This size detection method used by this component is inspired by Federico Zivolo's awesome [react-resize-aware][react-resize-aware-repo] project, which Atlassian has contributed a PR back to.

[react-resize-aware-repo]: https://github.com/FezVrasta/react-resize-aware
