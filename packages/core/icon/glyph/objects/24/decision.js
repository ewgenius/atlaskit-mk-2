'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24DecisionIcon = function Objects24DecisionIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 7.413l5 4.997V19a1 1 0 1 0 2 0v-7.004a1 1 0 0 0-.293-.707L7.415 6H10a1 1 0 0 0 0-2H5a1 1 0 0 0-1 1v5a1 1 0 1 0 2 0V7.412zM3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm15.25 4.297l-4.462 4.515a.999.999 0 0 0 .009 1.413.998.998 0 0 0 1.414-.008l4.462-4.514a1 1 0 0 0-1.423-1.406z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24DecisionIcon;