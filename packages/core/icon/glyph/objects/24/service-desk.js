'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24ServiceDeskIcon = function Objects24ServiceDeskIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M11 7v1.07A7.002 7.002 0 0 0 5 15v1h14v-1a7.002 7.002 0 0 0-6-6.93V7h1a1 1 0 0 0 0-2h-4a1 1 0 1 0 0 2h1zM3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm2 17a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2H5z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24ServiceDeskIcon;