'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24ConfigIcon = function Objects24ConfigIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm15.46 12c0-.806.518-1.483 1.237-1.734a.441.441 0 0 0 .29-.527 8.228 8.228 0 0 0-.769-1.833.44.44 0 0 0-.55-.18 1.877 1.877 0 0 1-1.187.075 1.828 1.828 0 0 1-1.29-1.284 1.875 1.875 0 0 1 .07-1.193.44.44 0 0 0-.18-.549 8.259 8.259 0 0 0-1.817-.76.44.44 0 0 0-.528.29 1.834 1.834 0 0 1-3.466 0 .443.443 0 0 0-.529-.29 8.219 8.219 0 0 0-1.843.775.432.432 0 0 0-.182.535 1.842 1.842 0 0 1-2.387 2.409.44.44 0 0 0-.547.18 8.228 8.228 0 0 0-.766 1.834.441.441 0 0 0 .283.526 1.834 1.834 0 0 1 0 3.452.44.44 0 0 0-.284.525c.163.577.388 1.128.665 1.646.11.205.363.275.574.178.547-.251 1.235-.243 1.935.219a.99.99 0 0 1 .278.277c.484.734.47 1.453.182 2.011a.435.435 0 0 0 .154.578 8.227 8.227 0 0 0 1.94.824c.222.063.45-.07.524-.287a1.834 1.834 0 0 1 1.736-1.245c.807 0 1.488.522 1.735 1.245.074.217.303.35.524.287a8.244 8.244 0 0 0 1.917-.812.435.435 0 0 0 .153-.58c-.292-.56-.31-1.281.176-2.019.07-.106.17-.206.278-.277.71-.47 1.404-.472 1.954-.212.21.1.467.03.578-.175.28-.518.504-1.07.67-1.649a.44.44 0 0 0-.29-.526A1.834 1.834 0 0 1 18.46 12zM7.402 12a4.6 4.6 0 1 1 9.2-.001 4.6 4.6 0 0 1-9.2.001z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24ConfigIcon;