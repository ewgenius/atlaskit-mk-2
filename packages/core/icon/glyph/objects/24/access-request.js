'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24AccessRequestIcon = function Objects24AccessRequestIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M8 9h-.994C5.897 9 5 9.9 5 11.009v7.982A2.01 2.01 0 0 0 7.006 21h9.988C18.103 21 19 20.1 19 18.991V11.01A2.009 2.009 0 0 0 16.994 9H16V7.002a4 4 0 1 0-8 0V9zM3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm9 5a2 2 0 0 1 2 2v2h-4V7a2 2 0 0 1 2-2zm-5 6h10v8H7v-8zm5 6a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24AccessRequestIcon;