'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16StoryIcon = function Objects16StoryIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm6 11l2.861 1.822c.42.38 1.139.111 1.139-.427v-8.19C16 7.54 15.403 7 14.667 7H9.333C8.596 7 8 7.539 8 8.206v8.19c0 .537.717.806 1.137.426L12 15zm0-2.371l2 1.274V9h-4v4.902l2-1.273z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16StoryIcon;