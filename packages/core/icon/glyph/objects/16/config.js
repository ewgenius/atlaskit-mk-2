'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16ConfigIcon = function Objects16ConfigIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm10.811 9.083a1.147 1.147 0 0 1 0-2.166.276.276 0 0 0 .18-.33 5.143 5.143 0 0 0-.48-1.146.274.274 0 0 0-.343-.112c-.222.09-.477.114-.742.046a1.143 1.143 0 0 1-.807-.802 1.172 1.172 0 0 1 .044-.745.275.275 0 0 0-.113-.344 5.162 5.162 0 0 0-1.135-.474.275.275 0 0 0-.33.18 1.147 1.147 0 0 1-2.166 0 .277.277 0 0 0-.33-.18 5.137 5.137 0 0 0-1.153.484.27.27 0 0 0-.113.334A1.151 1.151 0 0 1 7.83 9.333a.275.275 0 0 0-.342.113 5.142 5.142 0 0 0-.479 1.146.277.277 0 0 0 .177.33 1.146 1.146 0 0 1 0 2.157.275.275 0 0 0-.177.328c.101.36.242.705.415 1.029a.274.274 0 0 0 .359.11c.342-.156.772-.15 1.21.138a.63.63 0 0 1 .173.173c.303.459.294.908.114 1.257a.272.272 0 0 0 .096.36c.376.221.783.396 1.212.516a.272.272 0 0 0 .328-.18 1.147 1.147 0 0 1 2.17 0 .27.27 0 0 0 .327.18 5.16 5.16 0 0 0 1.198-.507.272.272 0 0 0 .096-.363c-.183-.35-.194-.8.11-1.262a.632.632 0 0 1 .173-.173c.444-.294.878-.295 1.222-.132a.274.274 0 0 0 .36-.11 5.13 5.13 0 0 0 .419-1.03.275.275 0 0 0-.18-.33zM9.5 12a2.5 2.5 0 1 1 5 0 2.5 2.5 0 0 1-5 0z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16ConfigIcon;