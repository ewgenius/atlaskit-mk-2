'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LockFilledIcon = function LockFilledIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fill-rule="evenodd"><path d="M5 11.009C5 9.899 5.897 9 7.006 9h9.988A2.01 2.01 0 0 1 19 11.009v7.982c0 1.11-.897 2.009-2.006 2.009H7.006A2.009 2.009 0 0 1 5 18.991V11.01zM12 17a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/><path d="M16 10h-2V7.002A2.001 2.001 0 0 0 12 5c-1.102 0-2 .898-2 2.002V10H8V7.002A4.004 4.004 0 0 1 12 3a4 4 0 0 1 4 4.002V10zm-8 0h2v1H8v-1zm6 0h2v1h-2v-1z" fill-rule="nonzero"/></g></svg>' }, props));
};
exports.default = LockFilledIcon;