# @atlaskit/global-search

## 1.2.0
- [minor] Remove environment prop and replace with explicit service url overrides. [b5bd020](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b5bd020)

## 1.1.1
- [patch] Support rendering of Jira results [0381f03](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0381f03)

## 1.1.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.0.1
- [patch] Update atlaskit/theme version [679e68a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/679e68a)

## 1.0.0
- [major] First release of global-search [7fcd54e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7fcd54e)
