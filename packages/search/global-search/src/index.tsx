export {
  default as GlobalQuickSearch,
} from './components/GlobalQuickSearchConfiguration';

export { Config } from './api/configureSearchClients';
