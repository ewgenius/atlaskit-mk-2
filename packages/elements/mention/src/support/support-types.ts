export interface MockMentionConfig {
  minWait?: number;
  maxWait?: number;
}
