# @atlaskit/editor-mobile-bridge

## 0.0.5

## 0.0.4

## 0.0.3
- [patch] Fix dependency [d4bcbf4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d4bcbf4)

## 0.0.2
- [patch] editor-mobile-bridge module introduced [4a338f6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4a338f6)
